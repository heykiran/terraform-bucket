resource "google_storage_bucket" "bucket" {
  name          = "${var.name}-1234567890-bucket-${var.env}"
  location      = "us-east1"
  force_destroy = true
}